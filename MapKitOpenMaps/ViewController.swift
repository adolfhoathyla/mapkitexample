//
//  ViewController.swift
//  MapKitOpenMaps
//
//  Created by Adolfho Athyla on 15/08/15.
//  Copyright (c) 2015 Adolfho Athyla. All rights reserved.
//

import UIKit
import MapKit

class ViewController: UIViewController, MKMapViewDelegate, CLLocationManagerDelegate {

    @IBOutlet var myMap: MKMapView!
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        initMyMap()
        
        addPlacesInMap(loadData())
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    private func initMyMap() {
        myMap.delegate = self
        myMap.setUserTrackingMode(.Follow, animated: true)
        
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        
    }
    
    private func loadData() -> NSArray? {
        let fileName = NSBundle.mainBundle().pathForResource("Benfica", ofType: "json")
        
        var readError: NSError?
        var data = NSData(contentsOfFile: fileName!, options: NSDataReadingOptions(0), error: &readError)
        
        var error: NSError?
        let jsonObject = NSJSONSerialization.JSONObjectWithData(data!, options: NSJSONReadingOptions(0), error: &error) as? NSDictionary
        
        let placesArray = jsonObject?["data"] as? NSArray
        
        return placesArray
    }
    
    private func addPlacesInMap(places: NSArray?) {
        
        if let places = places {
            for place in places {
                
                let placeAnnotation = MKPointAnnotation()
                
                placeAnnotation.title = place["name"] as? String
                placeAnnotation.subtitle = place["address"] as? String
                
                placeAnnotation.coordinate = CLLocationCoordinate2D(latitude: (place["latitude"] as? CLLocationDegrees)!, longitude: (place["longitude"] as? CLLocationDegrees)!)
                
                myMap.addAnnotation(placeAnnotation)
                
            }
        }
        
    }
    
    // MARK: - CoreLocation
    
    func locationManager(manager: CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        
        switch status {
        case .AuthorizedAlways:
            println("Autorização sempre")
            break
        case .AuthorizedWhenInUse:
            println("Autorização quando está em uso")
            break
        case .Denied:
            println("Autorização negada")
            break
        case .NotDetermined:
            println("Autorização não determinada")
            break
        case .Restricted:
            println("Autorização restrita")
            break
        }
        
    }
    
    // MARK: - MapKit
    
    func mapView(mapView: MKMapView!, viewForAnnotation annotation: MKAnnotation!) -> MKAnnotationView! {
        
        if annotation.isKindOfClass(MKUserLocation) {
            return nil
        }
        
        var placeAnnotation = myMap.dequeueReusableAnnotationViewWithIdentifier("placeAnnotation")
        
        if placeAnnotation == nil {
            placeAnnotation = MKAnnotationView(annotation: annotation, reuseIdentifier: "placeAnnotation")
            placeAnnotation.image = UIImage(named: "pin")
        }
        
        placeAnnotation.annotation = annotation
        placeAnnotation.rightCalloutAccessoryView = UIButton.buttonWithType(UIButtonType.DetailDisclosure) as? UIView
        
        placeAnnotation.canShowCallout = true
        
        return placeAnnotation
        
    }
    
    func mapView(mapView: MKMapView!, annotationView view: MKAnnotationView!, calloutAccessoryControlTapped control: UIControl!) {
        
        if control == view.rightCalloutAccessoryView {
            
            let alert = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
            
            alert.addAction(UIAlertAction(title: "Apple Maps", style: UIAlertActionStyle.Default, handler: { (alert) -> Void in
                
                let options = [ MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving ]
                
                let placemark = MKPlacemark(coordinate: view.annotation.coordinate, addressDictionary: nil)
                let mapItem = MKMapItem(placemark: placemark)
                mapItem.name = view.annotation.title
                
                mapItem.openInMapsWithLaunchOptions(options)
                
            }))
            
            alert.addAction(UIAlertAction(title: "Google Maps", style: UIAlertActionStyle.Default, handler: { (alert) -> Void in
                
                if UIApplication.sharedApplication().canOpenURL(NSURL(string: "comgooglemaps://")!) {
                    
                    let strMaps = "comgooglemaps://?saddr=\(view.annotation.coordinate.latitude),\(view.annotation.coordinate.longitude)&directionsmode=driving"
                    
                    println(strMaps)
                    
                    UIApplication.sharedApplication().openURL(NSURL(string: strMaps)!)
                    
                } else {
                    println("Can't use comgooglemaps")
                }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        
    }
    
    
    @IBAction func actionPrintScreen(sender: UIButton) {
        
        if UIScreen.mainScreen().respondsToSelector(Selector("scale")) {
            UIGraphicsBeginImageContextWithOptions(self.view.bounds.size, false, UIScreen.mainScreen().scale)
        } else {
            UIGraphicsBeginImageContext(self.view.bounds.size)
        }
        
        self.view.layer.renderInContext(UIGraphicsGetCurrentContext())
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        if image != nil {
            UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
            
            let alert = UIAlertController(title: "Success", message: "Your image was succefully saved", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Cancel, handler: nil))
            
            self.presentViewController(alert, animated: true, completion: nil)
            
        }
        
    }

}

